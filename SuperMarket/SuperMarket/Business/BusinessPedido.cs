﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Business
{
    class BusinessPedido
    {
        Database.DatabasePedido databasePedido = new Database.DatabasePedido();
        Business.BusinessPedidoItem businessPedidoItem = new BusinessPedidoItem();
        public List<Model.ModelPedido> BuscarPedido(string nomeRecebimento, int id, DateTime dia)
        {
            List<Model.ModelPedido> model = databasePedido.BuscarPedido(nomeRecebimento, id, dia);
            return model;
        }
        public List<Model.ModelPedidoItem> BuscarPorID(int id)
        {
            List<Model.ModelPedidoItem> lista = databasePedido.BuscarPorID(id);
            return lista;
        }

        public void SalvarPedido(Model.ModelPedido pedido, List<Model.ModelProdutoGridView> itens)
        {
            if (pedido.Complemento == "Complemento")
            {
                throw new ArgumentException("Informe um complemento");
            }
            if (pedido.ModoPagamento == "Modo de pagamento")
            {
                throw new ArgumentException("Informe um modo de pagamento");
            }
            if (pedido.NomeParaReceber == "Nome para receber")
            {
                throw new ArgumentException("Informe um nome de recebimento");
            }
            if (pedido.PedidoCep == "Cep")
            {
                throw new ArgumentException("Informe um cep");
            }
            if (pedido.PedidoEndereço == "Endereço")
            {
                throw new ArgumentException("Informe um endereço");
            }
            if (pedido.PedidoNumeroCasa == 0)
            {
                throw new ArgumentException("Informe o número de sua casa");
            }

            int pk = databasePedido.SalvarPedido(pedido);

            foreach (Model.ModelProdutoGridView item in itens)
            {
                Model.ModelPedidoItem pedidoItem = new Model.ModelPedidoItem();
                pedidoItem.IDPedido = pk;
                pedidoItem.IDProduto = item.Id;
                pedidoItem.Quantidade = item.Qtd;

                businessPedidoItem.SalvarItemPedido(pedidoItem);
            }
        }

        public List<Model.ModelPedido> BuscarParaDeletar(int id, DateTime data)
        {
            if (id == 0)
            {
                throw new ArgumentException("Informe um id válido");
            }

            List<Model.ModelPedido> model = databasePedido.BuscarParaDeletar(id, data);

            return model;
        }

        public void CancelarPedido(int id)
        {
            if (id == 0)
            {
                throw new ArgumentException("Informe um id válido");
            }

            databasePedido.CancelarPedido(id);
        }
        public void SalvarNovoPedido(int id, List<Model.ModelProdutoGridView> itens)
        {
            foreach (Model.ModelProdutoGridView item in itens)
            {
                Model.ModelPedidoItem pedidoItem = new Model.ModelPedidoItem();
                pedidoItem.IDProduto = item.Id;
                pedidoItem.Quantidade = item.Qtd;

                businessPedidoItem.SalvarItemPedido(pedidoItem);
            }
        }
    }
}
