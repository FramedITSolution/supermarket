﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Business
{
    class BusinessPedidoItem
    {
        public void AlterarQuantidade(int idproduto, int qtd)
        {
            if (idproduto == 0)
                throw new ArgumentException("ID do produto não pode ser zero");

            Database.DatabasePedidoItem databasePedidoItem = new Database.DatabasePedidoItem();
            databasePedidoItem.AlterarQuantidade(idproduto, qtd);
        }


        public void SalvarItemPedido(Model.ModelPedidoItem item)
        {
            Database.DatabasePedidoItem databasePedidoItem = new Database.DatabasePedidoItem();
            databasePedidoItem.SalvarPedido(item);

        }

        public void AlterarPedidoQTD(int idpedido, int idproduto, int idpedidoitem, int qtd)
        {
            if (idpedidoitem == 0)
                throw new ArgumentException("ID do pedido não pode ser zero");

            Database.DatabasePedidoItem databasePedidoItem = new Database.DatabasePedidoItem();
            databasePedidoItem.AlterarPedidoQTD(idpedido, idproduto, idpedidoitem, qtd);
        }
    }
}
