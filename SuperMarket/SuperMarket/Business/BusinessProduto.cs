﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Business
{
    class BusinessProduto
    {
        public List<Model.ModelProduto> BuscarProduto()
        {                        
            Database.DatabaseProduto databaseProduto = new Database.DatabaseProduto();
            List<Model.ModelProduto> model = databaseProduto.BuscarProduto();

            return model;
        }

        public Model.ModelProduto BuscaDosRegistros(string produto, string mercado)
        {
            Database.DatabaseProduto databaseProduto = new Database.DatabaseProduto();
            Model.ModelProduto modelProduto = databaseProduto.BuscaDosRegistros(produto, mercado);
            return modelProduto;
        }

        public List<Model.ModelProduto> Busca(string mercado)
        {
            Database.DatabaseProduto databaseProduto = new Database.DatabaseProduto();
            List<Model.ModelProduto> models = databaseProduto.Busca(mercado);
            return models;
        }
        
        public void AlterarPedido(string produto, decimal preco, string mercado, string marca)
        {
            Database.DatabaseProduto databaseProduto = new Database.DatabaseProduto();
            databaseProduto.AlterarPedido(produto, preco, mercado, marca);
        }
        public Model.ModelProduto BuscarPorID(int id)
        {
            Database.DatabaseProduto data = new Database.DatabaseProduto();
            Model.ModelProduto model = data.BuscarPorID(id);
            return model;
        }
    }
}
