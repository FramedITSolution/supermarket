﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Business
{
    class BusinessUsuario
    {
        Database.DatabaseUsuario databaseUsuario = new Database.DatabaseUsuario();

        public void InserirUsuario(Model.ModelUsuario usuario)
        {
            databaseUsuario.InserirUsuario(usuario);
        }

        public Model.ModelUsuario VerificarUsuario(string usuario, string senha, string email)
        {
            Model.ModelUsuario model = databaseUsuario.VerificarUsuario(usuario, senha, email);
            return model;
        }
    }
}
