﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Database
{
    class DatabasePedido
    {
        DB db = new DB();

        public void SalvarNovoPedido(int id, List<Model.ModelProdutoGridView> itens, int qtd, int idp)
        {
            string script = "update tb_pedido_item set id_produto = @id_produto, qtd_pedidoitem = @id_pedidoitem where id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));
            parms.Add(new MySqlParameter("qtd_pedidoitem", qtd));
            parms.Add(new MySqlParameter("id_produto", idp));
        }
        public int SalvarPedido(Model.ModelPedido pedido)
        {
            string script = @"insert into tb_pedido(dt_entrega, md_pagamento, pd_cep, pd_endereco, 
                 pd_nparareceber, pd_parcela, pd_ncasa, pd_complemento, pd_horadopedido, pd_total)
                 values (@dt_entrega, @md_pagamento, @pd_cep, @pd_endereco, @pd_nparareceber, 
                 @pd_parcela, @pd_ncasa, @pd_complemento, @pd_horadopedido, @pd_total)";
            // código para inserir o pedido

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_entrega", pedido.DataEntrega));
            parms.Add(new MySqlParameter("md_pagamento", pedido.ModoPagamento));
            parms.Add(new MySqlParameter("pd_cep", pedido.PedidoCep));
            parms.Add(new MySqlParameter("pd_endereco", pedido.PedidoEndereço));
            parms.Add(new MySqlParameter("pd_nparareceber", pedido.NomeParaReceber));
            parms.Add(new MySqlParameter("pd_parcela", pedido.PedidoParcela));
            parms.Add(new MySqlParameter("pd_ncasa", pedido.PedidoNumeroCasa));
            parms.Add(new MySqlParameter("pd_complemento", pedido.Complemento));
            parms.Add(new MySqlParameter("pd_horadopedido", pedido.Horadopedido));
            parms.Add(new MySqlParameter("pd_total", pedido.PedidoTotal));
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<Model.ModelPedido> BuscarPedido(string nomeRecebimento, int id, DateTime data)
        {
            string script = @"select * from tb_pedido where id_pedido = @id_pedido and 
                                            dt_entrega = @dt_entrega and pd_nparareceber like @pd_nparareceber";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));
            parms.Add(new MySqlParameter("dt_entrega", data));
            parms.Add(new MySqlParameter("pd_nparareceber", "%" + nomeRecebimento + "%"));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ModelPedido> lista = new List<Model.ModelPedido>();

            while (reader.Read())
            {
                Model.ModelPedido model = new Model.ModelPedido();
                model.IdPedido = Convert.ToInt32(reader["id_pedido"]);
                model.NomeParaReceber = Convert.ToString(reader["pd_nparareceber"]);
                model.DataEntrega = Convert.ToDateTime(reader["dt_entrega"]).Date;
                model.ModoPagamento = Convert.ToString(reader["md_pagamento"]);
                model.PedidoCep = Convert.ToString(reader["pd_cep"]);
                model.PedidoEndereço = Convert.ToString(reader["pd_endereco"]);
                model.PedidoNumeroCasa = Convert.ToInt32(reader["pd_ncasa"]);
                model.PedidoParcela = Convert.ToInt32(reader["pd_parcela"]);
                model.Complemento = Convert.ToString(reader["pd_complemento"]);
                model.Horadopedido = TimeSpan.Parse(reader["pd_horadopedido"].ToString());
                model.PedidoTotal = Convert.ToDecimal(reader["pd_total"]);

                lista.Add(model);
            }
            
            reader.Close();

            return lista;
        }

        public void DeletarPedido(string nomelista, DateTime datapedido)
        {
            string script = "delete from tb_pedido where nm_lista = @nm_lista, dt_entrega = @dt_entrega";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_lista", nomelista));
            parms.Add(new MySqlParameter("dt_entrega", datapedido));

            db.ExecuteInsertScript(script, parms);
        }

        public List<Model.ModelPedidoItem> BuscarPorID(int id)
        {
            string script = "select * from tb_pedido_item where id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));
            
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ModelPedidoItem> lista = new List<Model.ModelPedidoItem>();

            while(reader.Read())
            {
                Model.ModelPedidoItem model = new Model.ModelPedidoItem();
                model.ID = Convert.ToInt32(reader["id_pedidoitem"]);
                model.IDPedido = Convert.ToInt32(reader["id_pedido"]);
                model.IDProduto = Convert.ToInt32(reader["id_produto"]);
                model.Quantidade = Convert.ToInt32(reader["qtd_pedidoitem"]);
                lista.Add(model);
            }
            reader.Close();

            return lista;
        }

        public List<Model.ModelPedido> BuscarParaDeletar(int id, DateTime data)
        {
            string script = @"select * from tb_pedido where id_pedido = @id_pedido and 
                                                            dt_entrega = @dt_entrega";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));
            parms.Add(new MySqlParameter("dt_entrega", data));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ModelPedido> lista = new List<Model.ModelPedido>();

            while(reader.Read())
            {
                Model.ModelPedido model = new Model.ModelPedido();
                model.IdPedido = Convert.ToInt32(reader["id_pedido"]);
                model.NomeParaReceber = Convert.ToString(reader["pd_nparareceber"]);
                model.DataEntrega = Convert.ToDateTime(reader["dt_entrega"]).Date;
                model.ModoPagamento = Convert.ToString(reader["md_pagamento"]);
                model.PedidoCep = Convert.ToString(reader["pd_cep"]);
                model.PedidoEndereço = Convert.ToString(reader["pd_endereco"]);
                model.PedidoNumeroCasa = Convert.ToInt32(reader["pd_ncasa"]);
                model.PedidoParcela = Convert.ToInt32(reader["pd_parcela"]);
                model.Complemento = Convert.ToString(reader["pd_complemento"]);
                model.Horadopedido = TimeSpan.Parse(reader["pd_horadopedido"].ToString());
                model.PedidoTotal = Convert.ToDecimal(reader["pd_total"]);

                lista.Add(model);
            }

            reader.Close();

            return lista;
        }

        public void CancelarPedido(int id)
        {
            string script = "delete from tb_pedido where id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));

            db.ExecuteInsertScript(script, parms);
        }
    }
}
