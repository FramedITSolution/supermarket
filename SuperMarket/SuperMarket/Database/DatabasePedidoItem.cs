﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Database
{
    class DatabasePedidoItem
    {

        public void SalvarPedido(Model.ModelPedidoItem item)
        {
            string script = @"insert into tb_pedido_item(qtd_pedidoitem, id_produto, id_pedido)
                            values(@qtd_pedidoitem, @id_produto, @id_pedido)";
            // código para inserir pedido item

            List<MySqlParameter> parameters = new List<MySqlParameter>();
            parameters.Add(new MySqlParameter("qtd_pedidoitem", item.Quantidade));
            parameters.Add(new MySqlParameter("id_produto", item.IDProduto));
            parameters.Add(new MySqlParameter("id_pedido", item.IDPedido));

            DB db = new DB();
            db.ExecuteInsertScript(script, parameters);
        }

        public void AlterarQuantidade(int idproduto, int qtd)
        {
            string script = @"UPDATE tb_pedido_item SET qtd_pedidoitem = @qtd_pedidoitem, 
                                                        id_produto = @id_produto
                                                  WHERE id_pedido = @id_pedido
                                                  AND id_pedidoitem = @id_pedidoitem";

            List<MySqlParameter> parameters = new List<MySqlParameter>();
            parameters.Add(new MySqlParameter("id_produto", idproduto));
            parameters.Add(new MySqlParameter("qtd_pedidoitem", qtd)); //falta um parâmetro para 
                                                                       // o id_pedidoitem
            DB db = new DB();
            db.ExecuteInsertScript(script, parameters);
        }

        public void AlterarPedidoQTD(int idpedido, int idproduto, int idpedidoitem, int qtd)
        {
            string script = @"UPDATE tb_pedido_item SET qtd_pedidoitem = @qtd_pedidoitem, 
                                                        id_produto = @id_produto
                                                  WHERE id_pedido = @id_pedido
                                                  AND id_pedidoitem = @id_pedidoitem";

            List<MySqlParameter> parameters = new List<MySqlParameter>();
            parameters.Add(new MySqlParameter("id_pedido", idpedido));
            parameters.Add(new MySqlParameter("id_produto", idproduto));
            parameters.Add(new MySqlParameter("id_pedidoitem", idpedidoitem));
            parameters.Add(new MySqlParameter("qtd_pedidoitem", qtd));
            
            DB db = new DB();
            db.ExecuteInsertScript(script, parameters);
        }
    }
}
