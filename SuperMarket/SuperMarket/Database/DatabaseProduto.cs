﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Database
{
    class DatabaseProduto
    {
        public List<Model.ModelProduto> BuscarProduto ()
        {
            string script = @"select * from tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ModelProduto> model = new List<Model.ModelProduto>();

            while (reader.Read())
            {
                Model.ModelProduto modelProduto = new Model.ModelProduto();
                modelProduto.Id = Convert.ToInt32(reader["id_produto"]);
                modelProduto.NomeProduto = Convert.ToString(reader["nm_produto"]);
                modelProduto.Preco = Convert.ToDecimal(reader["pc_produto"]);
                modelProduto.Mercado = Convert.ToString(reader["mr_produto"]);
                modelProduto.Marca = Convert.ToString(reader["mc_produto"]);

                model.Add(modelProduto);
            }
            reader.Close();

            return model;
        }

        public void AlterarPedido(string produto, decimal preco, string mercado, string marca)
        {
            string script = @"update tb_produto set nm_produto = @nm_produto,
                                                    pc_produto = @pc_produto,                       
                                                    mr_produto = @mr_produto,
                                                    mc_produto = @mc_produto";

            List<MySqlParameter> parameters = new List<MySqlParameter>();
            parameters.Add(new MySqlParameter("nm_produto", produto));
            parameters.Add(new MySqlParameter("pc_produto", preco));
            parameters.Add(new MySqlParameter("mr_produto", mercado));
            parameters.Add(new MySqlParameter("mc_produto", marca));

            DB db = new DB();
            db.ExecuteInsertScript(script, parameters);
        }

        public Model.ModelProduto BuscaDosRegistros(string produto, string mercado)
        {
            string script = @"select * from tb_produto where mr_produto = @mr_produto and nm_produto = @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("mr_produto", mercado));
            parms.Add(new MySqlParameter("nm_produto", produto));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.ModelProduto lista = null;

            if (reader.Read())
            {
                lista = new Model.ModelProduto();
                lista.NomeProduto = Convert.ToString(reader["nm_produto"]);
                lista.Preco = Convert.ToDecimal(reader["pc_produto"]);
                lista.Mercado = Convert.ToString(reader["mr_produto"]);
                lista.Marca = Convert.ToString(reader["mc_produto"]);
            }
            reader.Close();

            return lista;
        }

        public List<Model.ModelProduto> Busca(string mercado)
        {
            string script = @"select * from tb_produto where mr_produto = @mr_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();


            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Model.ModelProduto> model = new List<Model.ModelProduto>();

            while (reader.Read())
            {
                Model.ModelProduto modelProduto = new Model.ModelProduto();
                modelProduto.Id = Convert.ToInt32(reader["id_produto"]);
                modelProduto.NomeProduto = Convert.ToString(reader["nm_produto"]);
                modelProduto.Preco = Convert.ToDecimal(reader["pc_produto"]);
                modelProduto.Mercado = Convert.ToString(reader["mr_produto"]);
                modelProduto.Marca = Convert.ToString(reader["mc_produto"]);

                model.Add(modelProduto);
            }
            reader.Close();

            return model;
        }
        public Model.ModelProduto BuscarPorID(int id)
        {
            string script = @"select * from tb_produto where id_produto= @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            DB db = new DB();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.ModelProduto lista = null;

            if (reader.Read())
            {
                lista = new Model.ModelProduto();
                lista.NomeProduto = Convert.ToString(reader["nm_produto"]);
                lista.Preco = Convert.ToDecimal(reader["pc_produto"]);
                lista.Mercado = Convert.ToString(reader["mr_produto"]);
                lista.Marca = Convert.ToString(reader["mc_produto"]);
            }
            reader.Close();

            return lista;
        }
    }
}
