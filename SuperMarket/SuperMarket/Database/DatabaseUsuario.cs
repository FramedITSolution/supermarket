﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Database
{
    class DatabaseUsuario
    {
        DB db = new DB();

        public void InserirUsuario(Model.ModelUsuario usuario)
        {
            string script = @"insert into tb_usuario
            (nm_usuario, sn_usuario, sb_usuario, cp_usuario, 
              tl_usuario, ml_usuario, nc_usuario, tp_cartao)
              values(@nm_usuario, @sn_usuario, @sb_usuario, 
             @cp_usuario, @tl_usuario, @ml_usuario, @nc_usuario, @tp_cartao);";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario.Nome));
            parms.Add(new MySqlParameter("sn_usuario", usuario.Senha));
            parms.Add(new MySqlParameter("sb_usuario", usuario.Sobrenome));
            parms.Add(new MySqlParameter("cp_usuario", usuario.Cep));
            parms.Add(new MySqlParameter("tl_usuario", usuario.Telefone));
            parms.Add(new MySqlParameter("ml_usuario", usuario.Email));
            parms.Add(new MySqlParameter("nc_usuario", usuario.NumeroCartão));
            parms.Add(new MySqlParameter("tp_cartao", usuario.CardType));

            
            db.ExecuteInsertScript(script, parms);
        }

        public Model.ModelUsuario VerificarUsuario(string usuario, string senha, string email)
        {
            string script = @"select * from tb_usuario where nm_usuario = @nm_usuario 
                             and sn_usuario = @sn_usuario and ml_usuario = @ml_usuario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", usuario));
            parms.Add(new MySqlParameter("sn_usuario", senha));
            parms.Add(new MySqlParameter("ml_usuario", email));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            Model.ModelUsuario model = null;

            if(reader.Read())
            {
                model = new Model.ModelUsuario();
                model.Id = Convert.ToInt32(reader["id_usuario"]);
                model.Nome = Convert.ToString(reader["nm_usuario"]);
                model.Sobrenome = Convert.ToString(reader["sn_usuario"]);
                model.Senha = Convert.ToString(reader["sn_usuario"]);
                model.Telefone = Convert.ToString(reader["tl_usuario"]);
                model.NumeroCartão = Convert.ToString(reader["nc_usuario"]);
                model.Email = Convert.ToString(reader["ml_usuario"]);
                model.Cep = Convert.ToString(reader["cp_usuario"]);
                model.CardType = Convert.ToString(reader["tp_cartao"]);
            }

            reader.Close();

            return model;

        }
    }
}

