﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Model
{
    class ModelPedido
    {
        public int IdPedido { get; set; }
        public string NomeLista { get; set; }
        public DateTime DataEntrega { get; set; }
        public string ModoPagamento { get; set; }
        public string PedidoCep { get; set; }
        public string PedidoEndereço { get; set; }
        public string NomeParaReceber { get; set; }
        public int PedidoParcela { get; set; }
        public int PedidoNumeroCasa { get; set; }
        public string Complemento { get; set; }
        public TimeSpan Horadopedido { get; set; }
        public decimal PedidoTotal { get; set; }

    }
}
