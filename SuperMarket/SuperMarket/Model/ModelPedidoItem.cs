﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Model
{
    class ModelPedidoItem
    {
        public  int ID { get; set; }
        public int Quantidade { get; set; }
        public int IDProduto { get; set; }
        public int IDPedido { get; set; }
    }
}
