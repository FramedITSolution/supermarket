﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarket.Model
{
    class ModelProduto
    {
        public int Id { get; set; }
        public string NomeProduto { get; set; }
        public decimal Preco { get; set; }
        public string Mercado { get; set; }
        public string Marca { get; set; }

    }

    class ModelProdutoGridView
    {
        public int Id { get; set; }
        public string NomeProduto { get; set; }
        public decimal Preco { get; set; }
        public string Mercado { get; set; }
        public string Marca { get; set; }
        public int Qtd { get; set; }
    }
}
