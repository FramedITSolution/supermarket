﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperMarket.Telas
{
    public partial class frmAlterarPedido : Form
    {
        public frmAlterarPedido()
        {
            InitializeComponent();
            this.CarregarProdudos();
        }

        Business.BusinessProduto businessProduto = new Business.BusinessProduto();
        Model.ModelProduto model = new Model.ModelProduto();
        Model.ModelPedidoItem pedidoitem = new Model.ModelPedidoItem();

        private void Consulta()
        {
            int id = Convert.ToInt32(txtID.Text);

            Business.BusinessPedido businessPedido = new Business.BusinessPedido();
            List <Model.ModelPedidoItem> lista = businessPedido.BuscarPorID(id);

            dgvLista.DataSource = lista;
        }

        private void CarregarProdudos()
        {
            List<Model.ModelProduto> model = businessProduto.BuscarProduto();


            cboProduto.DisplayMember = nameof(Model.ModelProduto.NomeProduto);
            cboProduto.DataSource = model;

            cboMercado.DisplayMember = nameof(Model.ModelProduto.Mercado);
            cboMercado.DataSource = model;

            string produto = cboProduto.Text;
            string mercado = cboMercado.Text;

            Model.ModelProduto modelo = businessProduto.BuscaDosRegistros(produto, mercado);
            txtMarca.Text = modelo.Marca;
            nudPreço.Value= modelo.Preco;
        }
        private void CalcularProduto()
        {
            string produto = cboProduto.Text;
            string mercado = cboMercado.Text;

            Model.ModelProduto model = businessProduto.BuscaDosRegistros(produto, mercado);
            decimal preco = model.Preco;
            txtMarca.Text = model.Marca;
            nudPreço.Value= preco;

            int qtd = Convert.ToInt32(nudQuantidade.Value);
            decimal totalPorProduto = preco * qtd;
            lblTotalPorProduto.Text = Convert.ToString(totalPorProduto);
        }

        private void label4_Click(object sender, EventArgs e)
        {
            Close();
            frmMenu start = new frmMenu();
            start.Show();
        }

        private void btnAddALista_Click(object sender, EventArgs e)
        {
            Model.ModelProduto p = cboProduto.SelectedItem as Model.ModelProduto;

            List<Model.ModelProdutoGridView> itens = dgvListaNova.DataSource as List<Model.ModelProdutoGridView> ?? new List<Model.ModelProdutoGridView>();
            itens.Add(new Model.ModelProdutoGridView
            {
                Id = p.Id,
                NomeProduto = p.NomeProduto,
                Marca = p.Marca,
                Mercado = p.Mercado,
                Preco = p.Preco,
                Qtd = Convert.ToInt32(nudQuantidade.Value)
            });
            dgvListaNova.DataSource = null;
            dgvListaNova.DataSource = itens;

            decimal totProduto = Convert.ToDecimal(lblTotalPorProduto.Text);
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            this.Consulta();
        }

        private void txtID_Enter(object sender, EventArgs e)
        {
            try
            {
                txtID.Text = string.Empty;
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
            
        }

        private void nudQuantidade_ValueChanged(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void cboMercado_MouseEnter(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void cboProduto_MouseEnter(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void groupBox2_Move(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void dgvLista_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dgvLista_RowDividerDoubleClick(object sender, DataGridViewRowDividerDoubleClickEventArgs e)
        {
            
        }

        private void dgvLista_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Model.ModelPedidoItem pedido = dgvLista.CurrentRow.DataBoundItem as Model.ModelPedidoItem;
                Business.BusinessProduto businessPedido = new Business.BusinessProduto();

                Model.ModelProduto model = businessPedido.BuscarPorID(pedido.IDProduto);

                txtMarca.Text = model.Marca;
                cboMercado.Text = model.Mercado;
                cboProduto.Text = model.NomeProduto;
                nudPreço.Value = model.Preco;
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnConcluido_Click(object sender, EventArgs e)
        {
            try
            {
                int qtd = Convert.ToInt32(nudQuantidade.Value);
                int id = Convert.ToInt32(txtID.Text);
                Business.BusinessPedidoItem businessPedido = new Business.BusinessPedidoItem();
                Model.ModelPedidoItem pedido = dgvLista.CurrentRow.DataBoundItem as Model.ModelPedidoItem;
                List<Model.ModelProdutoGridView> itens = dgvListaNova.DataSource as List<Model.ModelProdutoGridView> ?? new List<Model.ModelProdutoGridView>();
                pedidoitem = new Model.ModelPedidoItem();
                pedidoitem.ID = pedido.ID;
                pedidoitem.IDPedido = pedido.IDPedido;
                pedidoitem.IDProduto = pedido.IDProduto;
                pedidoitem.Quantidade = pedido.Quantidade;

                businessPedido.AlterarPedidoQTD(pedidoitem.IDPedido, pedidoitem.IDProduto, pedidoitem.ID, qtd);

                MessageBox.Show("Lista alterada com sucessso");
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
            
        }
    }
}
