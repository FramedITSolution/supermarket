﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperMarket.Telas
{
    public partial class frmCadastro : Form
    {
        public frmCadastro()
        {
            InitializeComponent();
        }

        private void btnCriarLogin_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin start = new frmLogin();
            start.Show();
        }

        private void txtCep_Enter(object sender, EventArgs e)
        {
            if (txtCep.Text == "Cep")
            {
                txtCep.Mask = "00000-000";
            }
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            if(txtCep.Text == "00000-000")
            {
                txtCep.Mask = string.Empty;
                txtCep.Text = "Cep";
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            txtUsuario.BackColor = Color.FromArgb(224, 224, 224);
            if (txtUsuario.Text == "Username" || txtUsuario.Text == string.Empty)
            {
                txtUsuario.Text = string.Empty;
                txtUsuario.ForeColor = Color.Black;
            }
            else
            {
                txtUsuario.ForeColor = Color.Black;
            }
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            txtUsuario.BackColor = Color.White;
            if (txtUsuario.Text == string.Empty)
            {
                txtUsuario.Text = "Username";
                txtUsuario.ForeColor = Color.Black;
                txtUsuario.BackColor = Color.FromArgb(224, 224, 224);
            }
            else
            {
                txtUsuario.ForeColor = Color.Black;
            }
        }

        private void txtSobrenome_Enter(object sender, EventArgs e)
        {
            txtSobrenome.BackColor = Color.FromArgb(224, 224, 224);
            if (txtSobrenome.Text == "Last name" || txtUsuario.Text == string.Empty)
            {
                txtSobrenome.Text = string.Empty;
                txtSobrenome.ForeColor = Color.Black;
            }
            else
            {
                txtSobrenome.ForeColor = Color.Black;
            }
        }

        private void txtSobrenome_Leave(object sender, EventArgs e)
        {
            if (txtSobrenome.Text == string.Empty)
            {
                txtSobrenome.Text = "Last name";
                txtSobrenome.ForeColor = Color.Black;
            }
            else
            {
                txtSobrenome.ForeColor = Color.Black;
            }
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            txtEmail.BackColor = Color.FromArgb(224, 224, 224);
            if (txtEmail.Text == "E-mail" || txtUsuario.Text == string.Empty)
            {
                txtEmail.Text = string.Empty;
                txtEmail.ForeColor = Color.Black;
            }
            else
            {
                txtEmail.ForeColor = Color.Black;
            }
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            txtEmail.BackColor = Color.White;
            if (txtEmail.Text == string.Empty)
            {
                txtEmail.Text = "E-mail";
                txtEmail.ForeColor = Color.Black;
            }
            else
            {
                txtEmail.ForeColor = Color.Black;
            }
        }

        private void txtNumeroCartao_Enter(object sender, EventArgs e)
        {
            txtNumeroCartao.BackColor = Color.FromArgb(224, 224, 224);
            if (txtNumeroCartao.Text == "Card number")
            {
                txtNumeroCartao.Text = string.Empty;
                txtNumeroCartao.ForeColor = Color.Black;
            }
            else
            {
                txtNumeroCartao.ForeColor = Color.Black;
            }
        }

        private void txtNumeroCartao_Leave(object sender, EventArgs e)
        {
            txtNumeroCartao.BackColor = Color.White;
            if (txtNumeroCartao.Text == string.Empty || txtNumeroCartao.Text == "Card number")
            {
                txtNumeroCartao.Text = "Card number";
                txtNumeroCartao.ForeColor = Color.Black;
            }
            else
            {
                txtNumeroCartao.ForeColor = Color.Black;
            }
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.FromArgb(224, 224, 224);
            if (txtSenha.Text == "Password" || txtSenha.Text == string.Empty)
            {
                txtSenha.Text = string.Empty;
                txtSenha.ForeColor = Color.Black;
                txtSenha.UseSystemPasswordChar = true;
            }
            else
            {
                txtSenha.ForeColor = Color.Black;
            }
        }

        private void txtSenha_Leave(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.White;
            if (txtSenha.Text == string.Empty)
            {
                txtSenha.UseSystemPasswordChar = false;
                txtSenha.Text = "Password";
                txtSenha.ForeColor = Color.Black;
            }
            else
            {
                txtSenha.ForeColor = Color.Black;
            }
        }

        private void txtConfirmacao_Enter(object sender, EventArgs e)
        {
            txtConfirmacao.BackColor = Color.FromArgb(224, 224, 224);
            if (txtConfirmacao.Text == "Confirm password" || txtSenha.Text == string.Empty)
            {
                txtConfirmacao.Text = string.Empty;
                txtConfirmacao.ForeColor = Color.Black;
                txtConfirmacao.UseSystemPasswordChar = true;
            }
            else
            {
                txtConfirmacao.ForeColor = Color.Black;
            }
        }

        private void txtConfirmacao_Leave(object sender, EventArgs e)
        {
            txtConfirmacao.BackColor = Color.White;
            if (txtConfirmacao.Text == string.Empty)
            {
                txtConfirmacao.UseSystemPasswordChar = false;
                txtConfirmacao.Text = "Confirm password";
                txtConfirmacao.ForeColor = Color.Black;
            }
            else
            {
                txtConfirmacao.ForeColor = Color.Black;
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validacoes();
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde", "SuperMarket");
            }
        }
        private void Validacoes()
        {
            bool arroba = txtEmail.Text.Contains("@");
            if(arroba == false)
            {
                throw new ArgumentException("Informe um e-mail válido");
            }
            bool usuario = false, senha = false, email = false, telefone = false, sobrenome = false;
            if (txtUsuario.Text == "Username" || txtUsuario.Text == string.Empty
                || txtSenha.Text == "Password" || txtSenha.Text == string.Empty
                || txtEmail.Text == "E-mail" || txtEmail.Text == string.Empty
                || txtTelefone.Text == "Telefone" || txtTelefone.Text == string.Empty)
            {
                if (txtUsuario.Text == "Username" || txtUsuario.Text == string.Empty)

                {
                    txtUsuario.BackColor = Color.FromArgb(224, 224, 224);
                    usuario = true;
                    txtUsuario.Text = "Username";

                }
                if (txtSobrenome.Text == "Last name" || txtSobrenome.Text == string.Empty)

                {
                    txtSobrenome.BackColor = Color.FromArgb(224, 224, 224);
                    sobrenome = true;
                    txtSobrenome.Text = "Last name";

                }
                if (txtSenha.Text == "Password" || txtSenha.Text == string.Empty)
                {
                    txtSenha.BackColor = Color.FromArgb(224, 224, 224);
                    senha = true;
                    txtSenha.Text = "Password";

                }
                if (txtEmail.Text == "E-mail" || txtEmail.Text == string.Empty)
                {
                    txtEmail.BackColor = Color.FromArgb(224, 224, 224);
                    email = true;
                    txtEmail.Text = "E-mail";

                }
                if (txtTelefone.Text == "Telefone" || txtTelefone.Text == string.Empty)
                {
                    txtTelefone.BackColor = Color.FromArgb(224, 224, 224); ;
                    telefone = true;
                    txtTelefone.Text = "Telefone";
                }
                if(txtSenha.Text != txtConfirmacao.Text)
                {
                    MessageBox.Show("Senha e confirmação de senha não correspondem","Register", MessageBoxButtons.OK);
                    return;
                }
                if (usuario == true || senha == true || email == true || telefone == true || sobrenome == true)
                {
                    MessageBox.Show("Preencha os campos necessários", "Register",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                Model.ModelUsuario model = new Model.ModelUsuario();
                model.Nome = txtUsuario.Text;
                model.Senha = txtSenha.Text;
                model.Sobrenome = txtSobrenome.Text;
                model.Telefone = txtTelefone.Text;
                model.Email = txtEmail.Text;
                model.NumeroCartão = txtNumeroCartao.Text;
                model.CardType = cboTipoCartao.Text;
                model.Cep = txtCep.Text;

                Business.BusinessUsuario businessUsuario = new Business.BusinessUsuario();
                businessUsuario.InserirUsuario(model);

                MessageBox.Show("Usuário cadastrado com sucesso!");

                Hide();
                frmLogin start = new frmLogin();
                start.Show();
            }
        }

        private void cboTipoCartao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtTelefone_Enter(object sender, EventArgs e)
        {
            txtTelefone.BackColor = Color.FromArgb(224, 224, 224);
            if (txtTelefone.Text == "Confirm password" || txtTelefone.Text == string.Empty)
            {
                txtTelefone.Text = string.Empty;
                txtTelefone.ForeColor = Color.Black;
            }
            else
            {
                txtTelefone.ForeColor = Color.Black;
            }
        }

        private void txtTelefone_Leave(object sender, EventArgs e)
        {
            txtTelefone.BackColor = Color.White;
            if (txtTelefone.Text == string.Empty)
            {
                txtTelefone.UseSystemPasswordChar = false;
                txtTelefone.Text = "Confirm password";
                txtTelefone.ForeColor = Color.Black;
            }
            else
            {
                txtTelefone.ForeColor = Color.Black;
            }
        }
    }
}


    
