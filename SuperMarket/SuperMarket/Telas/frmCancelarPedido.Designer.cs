﻿namespace SuperMarket.Telas
{
    partial class frmCancelarPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cancelamento = new System.Windows.Forms.GroupBox();
            this.lblFechar = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpData = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvListaParaCancelar = new System.Windows.Forms.DataGridView();
            this.txtID = new System.Windows.Forms.TextBox();
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.Cancelamento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaParaCancelar)).BeginInit();
            this.SuspendLayout();
            // 
            // Cancelamento
            // 
            this.Cancelamento.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Cancelamento.Controls.Add(this.lblFechar);
            this.Cancelamento.Controls.Add(this.label2);
            this.Cancelamento.Controls.Add(this.dtpData);
            this.Cancelamento.Controls.Add(this.label1);
            this.Cancelamento.Controls.Add(this.dgvListaParaCancelar);
            this.Cancelamento.Controls.Add(this.txtID);
            this.Cancelamento.Controls.Add(this.BtnCancelar);
            this.Cancelamento.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cancelamento.ForeColor = System.Drawing.Color.Black;
            this.Cancelamento.Location = new System.Drawing.Point(12, 12);
            this.Cancelamento.Name = "Cancelamento";
            this.Cancelamento.Size = new System.Drawing.Size(344, 319);
            this.Cancelamento.TabIndex = 0;
            this.Cancelamento.TabStop = false;
            this.Cancelamento.Text = "Cancelar pedido";
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.Black;
            this.lblFechar.Location = new System.Drawing.Point(317, 3);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 24);
            this.lblFechar.TabIndex = 27;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(18, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Data do pedido:";
            // 
            // dtpData
            // 
            this.dtpData.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpData.Location = new System.Drawing.Point(134, 61);
            this.dtpData.Name = "dtpData";
            this.dtpData.Size = new System.Drawing.Size(146, 25);
            this.dtpData.TabIndex = 8;
            this.dtpData.ValueChanged += new System.EventHandler(this.dtpData_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(27, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "ID do pedido:";
            // 
            // dgvListaParaCancelar
            // 
            this.dgvListaParaCancelar.AllowUserToAddRows = false;
            this.dgvListaParaCancelar.AllowUserToDeleteRows = false;
            this.dgvListaParaCancelar.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvListaParaCancelar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaParaCancelar.Location = new System.Drawing.Point(7, 107);
            this.dgvListaParaCancelar.Name = "dgvListaParaCancelar";
            this.dgvListaParaCancelar.ReadOnly = true;
            this.dgvListaParaCancelar.Size = new System.Drawing.Size(331, 155);
            this.dgvListaParaCancelar.TabIndex = 6;
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.White;
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtID.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.ForeColor = System.Drawing.Color.Black;
            this.txtID.Location = new System.Drawing.Point(134, 37);
            this.txtID.MaxLength = 2;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(146, 18);
            this.txtID.TabIndex = 3;
            this.txtID.TextChanged += new System.EventHandler(this.txtCancelarNLista_TextChanged);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.BackColor = System.Drawing.Color.DarkOrange;
            this.BtnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnCancelar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.ForeColor = System.Drawing.Color.Transparent;
            this.BtnCancelar.Location = new System.Drawing.Point(7, 279);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(332, 29);
            this.BtnCancelar.TabIndex = 0;
            this.BtnCancelar.Text = "Cancelar pedido";
            this.BtnCancelar.UseVisualStyleBackColor = false;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // frmCancelarPedido
            // 
            this.BackColor = System.Drawing.Color.DarkOrange;
            this.ClientSize = new System.Drawing.Size(368, 344);
            this.Controls.Add(this.Cancelamento);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCancelarPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Cancelamento.ResumeLayout(false);
            this.Cancelamento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaParaCancelar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpDataPedido;
        private System.Windows.Forms.Label lblPassouDoTempo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCancelarPedido;
        private System.Windows.Forms.DataGridView dgvLista;
        private System.Windows.Forms.GroupBox Cancelamento;
        private System.Windows.Forms.DataGridView dgvListaParaCancelar;
        private System.Windows.Forms.TextBox TxtDataPedido;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button BtnCancelar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFechar;
    }
}