﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperMarket.Telas
{
    public partial class frmCancelarPedido : Form
    {
        public frmCancelarPedido()
        {
            InitializeComponent();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Close();
            frmMenu start = new frmMenu();
            start.Show();
        }

        private void dtpData_ValueChanged(object sender, EventArgs e)
        {
            this.BuscarParaDeletar();
        }

        private void txtCancelarNLista_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.BuscarParaDeletar();

            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, informe um ID existente, em número");
            }
        }

        private void BuscarParaDeletar()
        {
            try
            {
                if (txtID.Text == null)
                {
                    throw new ArgumentException("Informe um ID");
                }
                int id = Convert.ToInt32(txtID.Text);
                DateTime data = dtpData.Value.Date;

                Business.BusinessPedido businessPedido = new Business.BusinessPedido();
                List<Model.ModelPedido> model = businessPedido.BuscarParaDeletar(id, data);

                dgvListaParaCancelar.DataSource = model;
            }
            catch (Exception)
            {

            }
            
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Model.ModelPedido pedido = dgvListaParaCancelar.CurrentRow.DataBoundItem as Model.ModelPedido; // esse

                TimeSpan dHora = pedido.Horadopedido - DateTime.Now.TimeOfDay;

                TimeSpan time = new TimeSpan(00, 20, 00);

                if (dHora > time)
                {
                    throw new ArgumentException("Você não pode cancelar esse pedido");
                }

                Business.BusinessPedido businessPedido = new Business.BusinessPedido();

                businessPedido.CancelarPedido(pedido.IdPedido);

                MessageBox.Show("Pedido cancelado");
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente novamente mais tarde",
                    "Cancelar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }           
        }
    }
}
