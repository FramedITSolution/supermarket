﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperMarket.Telas
{
    public partial class frmConsultarPedido : Form
    {
        public frmConsultarPedido()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
            frmMenu start = new frmMenu();
            start.Show();
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.Consultar();
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, informe um ID existente, em número");
            }
        }

        private void txtNomeRecebimento_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.Consultar();
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, informe um NOME DE RECEBIMENTO existente, em texto");
            }
        }

        private void dtpData_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.Consultar();
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, informe um ID existente, em número");
            }
        }

        private void Consultar()
        {
            try
            {
                if(txtID.Text == "ID do pedido")
                {
                    throw new ArgumentException("Informe um id para pesquisarmos");
                }
                int id = Convert.ToInt32(txtID.Text);
                string nomeRecebimento = txtNomeRecebimento.Text;
                DateTime data = dtpData.Value.Date;

                Business.BusinessPedido businessPedido = new Business.BusinessPedido();
                List<Model.ModelPedido> model = businessPedido.BuscarPedido(nomeRecebimento, id, data);

                dgvLista.DataSource = model;
            }
            catch(ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente novamente mais tarde", 
                    "Consultar", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }           
        }

        private void txtID_Enter(object sender, EventArgs e)
        {
            txtID.BackColor = Color.FromArgb(224, 224, 224);
            if(txtID.Text == string.Empty || txtID.Text == "ID do pedido")
            {
                txtID.Text = string.Empty;
            }
        }

        private void txtID_Leave(object sender, EventArgs e)
        {
            txtID.BackColor = Color.White;
            if(txtID.Text == string.Empty || txtID.Text == "ID do pedido")
            {
                txtID.Text = "ID do pedido";
            }
        }

        private void txtNomeRecebimento_Enter(object sender, EventArgs e)
        {
            txtNomeRecebimento.BackColor = Color.FromArgb(224, 224, 224);
            if (txtNomeRecebimento.Text == string.Empty || txtNomeRecebimento.Text == "Nome de recebimento")
            {
                txtNomeRecebimento.Text = string.Empty;
            }
        }

        private void txtNomeRecebimento_Leave(object sender, EventArgs e)
        {
            txtNomeRecebimento.BackColor = Color.White;
            if (txtNomeRecebimento.Text == string.Empty || txtNomeRecebimento.Text == "Nome de recebimento")
            {
                txtNomeRecebimento.Text = "Nome de recebimento";
            }
        }

        private void txtID_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                this.Consultar();
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, informe um ID existente, em número");
            }
        }
    }
}
