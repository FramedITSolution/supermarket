﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperMarket.Telas
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            try
            {
                Close();

            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validacoes();

            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }

        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCadastro start = new frmCadastro();
            start.Show();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        
        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            txtUsuario.BackColor = Color.FromArgb(224, 224, 224);
            if (txtUsuario.Text == "Username" || txtUsuario.Text == string.Empty)
            {
                txtUsuario.Text = string.Empty;
                txtUsuario.ForeColor = Color.Black;
            }
            else
            {
                txtUsuario.ForeColor = Color.Black;
            }
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.White;
            if (txtUsuario.Text == string.Empty)
            {
                txtUsuario.Text = "Username";
                txtUsuario.ForeColor = Color.Black;
            }
            else
            {
                txtUsuario.ForeColor = Color.Black;
            }
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.FromArgb(224, 224, 224);
            if (txtSenha.Text == "Password" || txtSenha.Text == string.Empty)
            {
                txtSenha.Text = string.Empty;
                txtSenha.ForeColor = Color.Black;
                txtSenha.UseSystemPasswordChar = true;
            }
            else
            {
                txtSenha.ForeColor = Color.Black;
            }
        }

        private void txtSenha_Leave(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.White;
            if (txtSenha.Text == string.Empty)
            {
                txtSenha.UseSystemPasswordChar = false;
                txtSenha.Text = "Password";
                txtSenha.ForeColor = Color.Black;
            }
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            txtEmail.BackColor = Color.FromArgb(224, 224, 224);
            if (txtEmail.Text == "E-mail" || txtEmail.Text == string.Empty)
            {
                txtEmail.Text = string.Empty;
                txtEmail.ForeColor = Color.Black;
            }
            else
            {
                txtEmail.ForeColor = Color.Black;
            }
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.White;
            if (txtEmail.Text == string.Empty)
            {
                txtEmail.Text = "E-mail";
                txtEmail.ForeColor = Color.Black;
            }
        }

        private void Validacoes()
        {
            string email1 = txtEmail.Text;
            bool arroba = email1.Contains("@");

            bool usuario = true, senha = true, email = false;
            if (txtUsuario.Text == "Username" || txtUsuario.Text == string.Empty 
                || txtSenha.Text == "Password" || txtSenha.Text == string.Empty
                || email1 == "E-mail" || email1 == string.Empty)
            {
                if (txtUsuario.Text == "Username" || txtUsuario.Text == string.Empty)
                {
                    txtUsuario.BackColor = Color.FromArgb(224, 224, 224);
                    usuario = true;
                    txtUsuario.Text = "Username";

                }
                if (txtSenha.Text == "Password" || txtSenha.Text == string.Empty)
                {
                    txtSenha.BackColor = Color.FromArgb(224, 224, 224);
                    senha = true;
                    txtSenha.Text = "Password";

                }
                if (txtEmail.Text == "E-mail" || txtEmail.Text == string.Empty)
                {
                    txtEmail.BackColor = Color.FromArgb(224, 224, 224);
                    email = true;
                    txtEmail.Text = "E-mail";

                }
                else if(arroba == false)
                {
                    MessageBox.Show("Informe um e-mail válido");
                    return;
                }
                else if (usuario == true || senha == true || email == true)
                {
                    MessageBox.Show("Preencha os campos necessários", "Login",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                this.VerificarUsuario();
            }
        }

        private void VerificarUsuario()
        {
            string usuario = txtUsuario.Text;
            string senha = txtSenha.Text;
            string email = txtEmail.Text;

            Business.BusinessUsuario businessUsuario = new Business.BusinessUsuario();
            Model.ModelUsuario model = businessUsuario.VerificarUsuario(usuario, senha, email);

            if(model != null)
            {
                Hide();
                frmMenu start = new frmMenu();
                start.Show();
            }
            else
            {
                MessageBox.Show("Usuário não existente", "SuperMarket", MessageBoxButtons.OK);
            }
        }

        private void btnRegistrar_Click_1(object sender, EventArgs e)
        {
            try
            {
                Hide();
                frmCadastro start = new frmCadastro();
                start.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde");
            }
        }
    }
}
