﻿namespace SuperMarket.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.imgVoltarLogin = new System.Windows.Forms.PictureBox();
            this.imgRemoverPedido = new System.Windows.Forms.PictureBox();
            this.imgAlterarPedido = new System.Windows.Forms.PictureBox();
            this.imgConsultarPedido = new System.Windows.Forms.PictureBox();
            this.imgInserirPedido = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltarLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRemoverPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAlterarPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarPedido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInserirPedido)).BeginInit();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(23, 256);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(122, 22);
            this.label.TabIndex = 1;
            this.label.Text = "Fazer pedido";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(-2, -5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(558, 143);
            this.panel1.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkOrange;
            this.pictureBox1.Image = global::SuperMarket.Properties.Resources.logo;
            this.pictureBox1.Location = new System.Drawing.Point(151, -73);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(275, 275);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(202, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 22);
            this.label2.TabIndex = 4;
            this.label2.Text = "Consultar pedido";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(394, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 22);
            this.label5.TabIndex = 10;
            this.label5.Text = "Alterar pedido";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 400);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 22);
            this.label6.TabIndex = 12;
            this.label6.Text = "Cancelar pedido";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(328, 400);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 22);
            this.label1.TabIndex = 18;
            this.label1.Text = "Voltar ao login";
            // 
            // imgVoltarLogin
            // 
            this.imgVoltarLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgVoltarLogin.Image = global::SuperMarket.Properties.Resources.voltar;
            this.imgVoltarLogin.Location = new System.Drawing.Point(346, 313);
            this.imgVoltarLogin.Name = "imgVoltarLogin";
            this.imgVoltarLogin.Size = new System.Drawing.Size(91, 84);
            this.imgVoltarLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgVoltarLogin.TabIndex = 15;
            this.imgVoltarLogin.TabStop = false;
            this.imgVoltarLogin.Click += new System.EventHandler(this.imgVoltarLogin_Click);
            // 
            // imgRemoverPedido
            // 
            this.imgRemoverPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgRemoverPedido.Image = global::SuperMarket.Properties.Resources.cancelar;
            this.imgRemoverPedido.Location = new System.Drawing.Point(90, 313);
            this.imgRemoverPedido.Name = "imgRemoverPedido";
            this.imgRemoverPedido.Size = new System.Drawing.Size(91, 84);
            this.imgRemoverPedido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgRemoverPedido.TabIndex = 13;
            this.imgRemoverPedido.TabStop = false;
            this.imgRemoverPedido.Click += new System.EventHandler(this.imgRemoverPedido_Click);
            // 
            // imgAlterarPedido
            // 
            this.imgAlterarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgAlterarPedido.Image = global::SuperMarket.Properties.Resources.lapis;
            this.imgAlterarPedido.Location = new System.Drawing.Point(413, 178);
            this.imgAlterarPedido.Name = "imgAlterarPedido";
            this.imgAlterarPedido.Size = new System.Drawing.Size(88, 75);
            this.imgAlterarPedido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAlterarPedido.TabIndex = 11;
            this.imgAlterarPedido.TabStop = false;
            this.imgAlterarPedido.Click += new System.EventHandler(this.imgAlterarPedido_Click);
            // 
            // imgConsultarPedido
            // 
            this.imgConsultarPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgConsultarPedido.Image = global::SuperMarket.Properties.Resources.imglupa;
            this.imgConsultarPedido.Location = new System.Drawing.Point(233, 178);
            this.imgConsultarPedido.Name = "imgConsultarPedido";
            this.imgConsultarPedido.Size = new System.Drawing.Size(88, 75);
            this.imgConsultarPedido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgConsultarPedido.TabIndex = 5;
            this.imgConsultarPedido.TabStop = false;
            this.imgConsultarPedido.Click += new System.EventHandler(this.imgConsultarPedido_Click);
            // 
            // imgInserirPedido
            // 
            this.imgInserirPedido.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgInserirPedido.Image = global::SuperMarket.Properties.Resources.Carrinho_de_Compras;
            this.imgInserirPedido.Location = new System.Drawing.Point(34, 178);
            this.imgInserirPedido.Name = "imgInserirPedido";
            this.imgInserirPedido.Size = new System.Drawing.Size(88, 75);
            this.imgInserirPedido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgInserirPedido.TabIndex = 2;
            this.imgInserirPedido.TabStop = false;
            this.imgInserirPedido.Click += new System.EventHandler(this.imgInserirPedido_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 455);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgVoltarLogin);
            this.Controls.Add(this.imgRemoverPedido);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.imgAlterarPedido);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.imgConsultarPedido);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imgInserirPedido);
            this.Controls.Add(this.label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenu";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgVoltarLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgRemoverPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAlterarPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgConsultarPedido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInserirPedido)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.PictureBox imgInserirPedido;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox imgConsultarPedido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox imgAlterarPedido;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox imgVoltarLogin;
        private System.Windows.Forms.PictureBox imgRemoverPedido;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}