﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperMarket.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void imgInserirPedido_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmPedido start = new frmPedido();
            start.Show();
        }

        private void imgConsultarPedido_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmConsultarPedido start = new frmConsultarPedido();
            start.Show();
        }

        private void imgAlterarPedido_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmAlterarPedido start = new frmAlterarPedido();
            start.Show();
        }

        private void imgFecharApp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imgVoltarLogin_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin start = new frmLogin();
            start.Show();
        }

        private void imgRemoverPedido_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCancelarPedido start = new frmCancelarPedido();
            start.Show();
        }

        
    }
}
