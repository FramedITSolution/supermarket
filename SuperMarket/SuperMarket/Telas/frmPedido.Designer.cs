﻿namespace SuperMarket.Telas
{
    partial class frmPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpoEscolherPedido = new System.Windows.Forms.GroupBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtMarca = new System.Windows.Forms.TextBox();
            this.cboMercado = new System.Windows.Forms.ComboBox();
            this.cboProduto = new System.Windows.Forms.ComboBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.lblTotalPorProduto = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudQuantidade = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtNomeRecebimento = new System.Windows.Forms.TextBox();
            this.txtCep = new System.Windows.Forms.TextBox();
            this.dtpData = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cboModoDePagamento = new System.Windows.Forms.ComboBox();
            this.txtNumeroCartao = new System.Windows.Forms.TextBox();
            this.nudParcelas = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvLista = new System.Windows.Forms.DataGridView();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblFechar = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gpoEscolherPedido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantidade)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudParcelas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gpoEscolherPedido
            // 
            this.gpoEscolherPedido.BackColor = System.Drawing.Color.White;
            this.gpoEscolherPedido.Controls.Add(this.panel12);
            this.gpoEscolherPedido.Controls.Add(this.panel11);
            this.gpoEscolherPedido.Controls.Add(this.panel1);
            this.gpoEscolherPedido.Controls.Add(this.panel2);
            this.gpoEscolherPedido.Controls.Add(this.txtMarca);
            this.gpoEscolherPedido.Controls.Add(this.cboMercado);
            this.gpoEscolherPedido.Controls.Add(this.cboProduto);
            this.gpoEscolherPedido.Controls.Add(this.btnRegistrar);
            this.gpoEscolherPedido.Controls.Add(this.txtPreco);
            this.gpoEscolherPedido.Controls.Add(this.lblTotalPorProduto);
            this.gpoEscolherPedido.Controls.Add(this.label2);
            this.gpoEscolherPedido.Controls.Add(this.nudQuantidade);
            this.gpoEscolherPedido.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gpoEscolherPedido.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpoEscolherPedido.Location = new System.Drawing.Point(14, 70);
            this.gpoEscolherPedido.Margin = new System.Windows.Forms.Padding(4);
            this.gpoEscolherPedido.Name = "gpoEscolherPedido";
            this.gpoEscolherPedido.Padding = new System.Windows.Forms.Padding(4);
            this.gpoEscolherPedido.Size = new System.Drawing.Size(275, 233);
            this.gpoEscolherPedido.TabIndex = 0;
            this.gpoEscolherPedido.TabStop = false;
            this.gpoEscolherPedido.Text = "Escolher pedido";
            this.gpoEscolherPedido.Move += new System.EventHandler(this.gpoEscolherPedido_Move);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel12.ForeColor = System.Drawing.Color.Black;
            this.panel12.Location = new System.Drawing.Point(12, 65);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(133, 1);
            this.panel12.TabIndex = 36;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel11.ForeColor = System.Drawing.Color.Black;
            this.panel11.Location = new System.Drawing.Point(12, 136);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(130, 1);
            this.panel11.TabIndex = 36;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(12, 170);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(133, 1);
            this.panel1.TabIndex = 35;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel2.ForeColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(12, 98);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(133, 1);
            this.panel2.TabIndex = 33;
            // 
            // txtMarca
            // 
            this.txtMarca.BackColor = System.Drawing.Color.White;
            this.txtMarca.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMarca.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMarca.Enabled = false;
            this.txtMarca.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarca.ForeColor = System.Drawing.Color.Black;
            this.txtMarca.Location = new System.Drawing.Point(12, 148);
            this.txtMarca.Name = "txtMarca";
            this.txtMarca.Size = new System.Drawing.Size(133, 18);
            this.txtMarca.TabIndex = 34;
            this.txtMarca.TabStop = false;
            this.txtMarca.Text = "Marca";
            // 
            // cboMercado
            // 
            this.cboMercado.BackColor = System.Drawing.Color.White;
            this.cboMercado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboMercado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboMercado.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboMercado.ForeColor = System.Drawing.Color.Black;
            this.cboMercado.FormattingEnabled = true;
            this.cboMercado.Location = new System.Drawing.Point(13, 34);
            this.cboMercado.Name = "cboMercado";
            this.cboMercado.Size = new System.Drawing.Size(132, 25);
            this.cboMercado.TabIndex = 31;
            this.cboMercado.Text = "Mercado";
            this.cboMercado.SelectedIndexChanged += new System.EventHandler(this.cboMercado_SelectedIndexChanged);
            this.cboMercado.MouseEnter += new System.EventHandler(this.cboMercado_MouseEnter);
            this.cboMercado.MouseLeave += new System.EventHandler(this.cboMercado_MouseLeave);
            // 
            // cboProduto
            // 
            this.cboProduto.BackColor = System.Drawing.Color.White;
            this.cboProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboProduto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboProduto.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProduto.ForeColor = System.Drawing.Color.Black;
            this.cboProduto.FormattingEnabled = true;
            this.cboProduto.Location = new System.Drawing.Point(13, 105);
            this.cboProduto.Name = "cboProduto";
            this.cboProduto.Size = new System.Drawing.Size(132, 25);
            this.cboProduto.TabIndex = 30;
            this.cboProduto.Text = "Produto";
            this.cboProduto.MouseEnter += new System.EventHandler(this.cboProduto_MouseEnter);
            this.cboProduto.MouseLeave += new System.EventHandler(this.cboProduto_MouseLeave);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistrar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnRegistrar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrar.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.btnRegistrar.Location = new System.Drawing.Point(11, 187);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(250, 27);
            this.btnRegistrar.TabIndex = 12;
            this.btnRegistrar.Text = "Adicionar a lista";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // txtPreco
            // 
            this.txtPreco.BackColor = System.Drawing.Color.White;
            this.txtPreco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPreco.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPreco.Enabled = false;
            this.txtPreco.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreco.ForeColor = System.Drawing.Color.Black;
            this.txtPreco.Location = new System.Drawing.Point(12, 76);
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.Size = new System.Drawing.Size(133, 18);
            this.txtPreco.TabIndex = 32;
            this.txtPreco.TabStop = false;
            this.txtPreco.Text = "Preço";
            // 
            // lblTotalPorProduto
            // 
            this.lblTotalPorProduto.Location = new System.Drawing.Point(151, 115);
            this.lblTotalPorProduto.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalPorProduto.Name = "lblTotalPorProduto";
            this.lblTotalPorProduto.Size = new System.Drawing.Size(103, 39);
            this.lblTotalPorProduto.TabIndex = 7;
            this.lblTotalPorProduto.Text = "--";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(151, 76);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 39);
            this.label2.TabIndex = 6;
            this.label2.Text = "Total pelo produto:";
            // 
            // nudQuantidade
            // 
            this.nudQuantidade.Location = new System.Drawing.Point(189, 38);
            this.nudQuantidade.Margin = new System.Windows.Forms.Padding(4);
            this.nudQuantidade.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudQuantidade.Name = "nudQuantidade";
            this.nudQuantidade.Size = new System.Drawing.Size(64, 25);
            this.nudQuantidade.TabIndex = 5;
            this.nudQuantidade.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudQuantidade.ValueChanged += new System.EventHandler(this.nudQuantidade_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.panel13);
            this.groupBox2.Controls.Add(this.txtNumCasa);
            this.groupBox2.Controls.Add(this.panel9);
            this.groupBox2.Controls.Add(this.panel6);
            this.groupBox2.Controls.Add(this.panel5);
            this.groupBox2.Controls.Add(this.txtComplemento);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.txtEndereco);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Controls.Add(this.txtNomeRecebimento);
            this.groupBox2.Controls.Add(this.txtCep);
            this.groupBox2.Controls.Add(this.dtpData);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(14, 339);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(333, 183);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Entrega";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel13.ForeColor = System.Drawing.Color.Black;
            this.panel13.Location = new System.Drawing.Point(170, 146);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(133, 1);
            this.panel13.TabIndex = 44;
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.BackColor = System.Drawing.Color.White;
            this.txtNumCasa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumCasa.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNumCasa.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCasa.ForeColor = System.Drawing.Color.Black;
            this.txtNumCasa.Location = new System.Drawing.Point(170, 124);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(133, 18);
            this.txtNumCasa.TabIndex = 43;
            this.txtNumCasa.TabStop = false;
            this.txtNumCasa.Text = "Número da casa";
            this.txtNumCasa.Enter += new System.EventHandler(this.txtNumCasa_Enter);
            this.txtNumCasa.Leave += new System.EventHandler(this.txtNumCasa_Leave);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel9.ForeColor = System.Drawing.Color.Black;
            this.panel9.Location = new System.Drawing.Point(11, 113);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(130, 1);
            this.panel9.TabIndex = 42;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel6.ForeColor = System.Drawing.Color.Black;
            this.panel6.Location = new System.Drawing.Point(170, 114);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(133, 1);
            this.panel6.TabIndex = 41;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel5.ForeColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(12, 146);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(133, 1);
            this.panel5.TabIndex = 39;
            // 
            // txtComplemento
            // 
            this.txtComplemento.BackColor = System.Drawing.Color.White;
            this.txtComplemento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtComplemento.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtComplemento.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtComplemento.Location = new System.Drawing.Point(170, 92);
            this.txtComplemento.MaxLength = 50;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(133, 18);
            this.txtComplemento.TabIndex = 40;
            this.txtComplemento.TabStop = false;
            this.txtComplemento.Text = "Complemento";
            this.txtComplemento.Enter += new System.EventHandler(this.txtComplemento_Enter);
            this.txtComplemento.Leave += new System.EventHandler(this.txtComplemento_Leave);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel4.ForeColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(12, 73);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(133, 1);
            this.panel4.TabIndex = 37;
            // 
            // txtEndereco
            // 
            this.txtEndereco.BackColor = System.Drawing.Color.White;
            this.txtEndereco.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEndereco.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEndereco.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.ForeColor = System.Drawing.Color.Black;
            this.txtEndereco.Location = new System.Drawing.Point(12, 124);
            this.txtEndereco.MaxLength = 150;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(133, 18);
            this.txtEndereco.TabIndex = 38;
            this.txtEndereco.TabStop = false;
            this.txtEndereco.Text = "Endereço";
            this.txtEndereco.Enter += new System.EventHandler(this.txtEndereco_Enter);
            this.txtEndereco.Leave += new System.EventHandler(this.txtEndereco_Leave);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel3.ForeColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(170, 73);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(133, 1);
            this.panel3.TabIndex = 35;
            // 
            // txtNomeRecebimento
            // 
            this.txtNomeRecebimento.BackColor = System.Drawing.Color.White;
            this.txtNomeRecebimento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNomeRecebimento.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNomeRecebimento.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomeRecebimento.ForeColor = System.Drawing.Color.Black;
            this.txtNomeRecebimento.Location = new System.Drawing.Point(12, 51);
            this.txtNomeRecebimento.MaxLength = 50;
            this.txtNomeRecebimento.Name = "txtNomeRecebimento";
            this.txtNomeRecebimento.Size = new System.Drawing.Size(133, 18);
            this.txtNomeRecebimento.TabIndex = 36;
            this.txtNomeRecebimento.TabStop = false;
            this.txtNomeRecebimento.Text = "Nome para receber";
            this.txtNomeRecebimento.Enter += new System.EventHandler(this.txtNomeRecebimento_Enter);
            this.txtNomeRecebimento.Leave += new System.EventHandler(this.txtNomeRecebimento_Leave);
            // 
            // txtCep
            // 
            this.txtCep.BackColor = System.Drawing.Color.White;
            this.txtCep.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCep.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCep.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCep.ForeColor = System.Drawing.Color.Black;
            this.txtCep.Location = new System.Drawing.Point(170, 51);
            this.txtCep.MaxLength = 11;
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(133, 18);
            this.txtCep.TabIndex = 34;
            this.txtCep.TabStop = false;
            this.txtCep.Text = "Cep";
            this.txtCep.Enter += new System.EventHandler(this.txtCep_Enter);
            this.txtCep.Leave += new System.EventHandler(this.txtCep_Leave);
            // 
            // dtpData
            // 
            this.dtpData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpData.Location = new System.Drawing.Point(11, 85);
            this.dtpData.Margin = new System.Windows.Forms.Padding(4);
            this.dtpData.Name = "dtpData";
            this.dtpData.Size = new System.Drawing.Size(129, 25);
            this.dtpData.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.panel10);
            this.groupBox3.Controls.Add(this.panel8);
            this.groupBox3.Controls.Add(this.txtSenha);
            this.groupBox3.Controls.Add(this.panel7);
            this.groupBox3.Controls.Add(this.cboModoDePagamento);
            this.groupBox3.Controls.Add(this.txtNumeroCartao);
            this.groupBox3.Controls.Add(this.nudParcelas);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(355, 339);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(267, 183);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pagamento";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel10.ForeColor = System.Drawing.Color.Black;
            this.panel10.Location = new System.Drawing.Point(14, 96);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(230, 1);
            this.panel10.TabIndex = 42;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel8.ForeColor = System.Drawing.Color.Black;
            this.panel8.Location = new System.Drawing.Point(12, 161);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(133, 1);
            this.panel8.TabIndex = 43;
            // 
            // txtSenha
            // 
            this.txtSenha.BackColor = System.Drawing.Color.White;
            this.txtSenha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSenha.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtSenha.Enabled = false;
            this.txtSenha.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.ForeColor = System.Drawing.Color.Black;
            this.txtSenha.Location = new System.Drawing.Point(12, 139);
            this.txtSenha.MaxLength = 16;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(133, 18);
            this.txtSenha.TabIndex = 42;
            this.txtSenha.TabStop = false;
            this.txtSenha.Text = "Password";
            this.txtSenha.Enter += new System.EventHandler(this.txtSenha_Enter);
            this.txtSenha.Leave += new System.EventHandler(this.txtSenha_Leave);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel7.ForeColor = System.Drawing.Color.Black;
            this.panel7.Location = new System.Drawing.Point(12, 128);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(133, 1);
            this.panel7.TabIndex = 41;
            // 
            // cboModoDePagamento
            // 
            this.cboModoDePagamento.BackColor = System.Drawing.Color.White;
            this.cboModoDePagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboModoDePagamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboModoDePagamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboModoDePagamento.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboModoDePagamento.ForeColor = System.Drawing.Color.Black;
            this.cboModoDePagamento.FormattingEnabled = true;
            this.cboModoDePagamento.Items.AddRange(new object[] {
            "Cartão",
            "Boleto",
            "Dinheiro"});
            this.cboModoDePagamento.Location = new System.Drawing.Point(11, 70);
            this.cboModoDePagamento.Name = "cboModoDePagamento";
            this.cboModoDePagamento.Size = new System.Drawing.Size(233, 25);
            this.cboModoDePagamento.TabIndex = 32;
            this.cboModoDePagamento.SelectedIndexChanged += new System.EventHandler(this.cboModoDePagamento_SelectedIndexChanged);
            // 
            // txtNumeroCartao
            // 
            this.txtNumeroCartao.BackColor = System.Drawing.Color.White;
            this.txtNumeroCartao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumeroCartao.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNumeroCartao.Enabled = false;
            this.txtNumeroCartao.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeroCartao.ForeColor = System.Drawing.Color.Black;
            this.txtNumeroCartao.Location = new System.Drawing.Point(12, 106);
            this.txtNumeroCartao.MaxLength = 50;
            this.txtNumeroCartao.Name = "txtNumeroCartao";
            this.txtNumeroCartao.Size = new System.Drawing.Size(133, 18);
            this.txtNumeroCartao.TabIndex = 40;
            this.txtNumeroCartao.TabStop = false;
            this.txtNumeroCartao.Text = "Card number";
            this.txtNumeroCartao.Enter += new System.EventHandler(this.txtNumeroCartao_Enter);
            this.txtNumeroCartao.Leave += new System.EventHandler(this.txtNumeroCartao_Leave);
            // 
            // nudParcelas
            // 
            this.nudParcelas.Location = new System.Drawing.Point(172, 31);
            this.nudParcelas.Margin = new System.Windows.Forms.Padding(4);
            this.nudParcelas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudParcelas.Name = "nudParcelas";
            this.nudParcelas.Size = new System.Drawing.Size(75, 25);
            this.nudParcelas.TabIndex = 8;
            this.nudParcelas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Parcelas s. júros";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(47, 551);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 34);
            this.label6.TabIndex = 8;
            this.label6.Text = "Total:";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(198, 580);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(31, 34);
            this.lblTotal.TabIndex = 8;
            this.lblTotal.Text = "0";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(498, 542);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 99);
            this.label8.TabIndex = 8;
            this.label8.Text = "Você só pode cancelar o pedido dentro de 20 minutos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(386, 82);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Itens do seu pedido";
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToAddRows = false;
            this.dgvLista.AllowUserToDeleteRows = false;
            this.dgvLista.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLista.GridColor = System.Drawing.SystemColors.WindowFrame;
            this.dgvLista.Location = new System.Drawing.Point(303, 104);
            this.dgvLista.Margin = new System.Windows.Forms.Padding(5);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.ReadOnly = true;
            this.dgvLista.Size = new System.Drawing.Size(319, 196);
            this.dgvLista.TabIndex = 10;
            // 
            // btnLogin
            // 
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLogin.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(146, 643);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(357, 45);
            this.btnLogin.TabIndex = 11;
            this.btnLogin.Text = "Fazer pedido";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.Location = new System.Drawing.Point(599, 9);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(24, 24);
            this.lblFechar.TabIndex = 28;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click_1);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SuperMarket.Properties.Resources.logo_SuperMarket;
            this.pictureBox1.Location = new System.Drawing.Point(245, -21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 90);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // frmPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOrange;
            this.ClientSize = new System.Drawing.Size(635, 696);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.dgvLista);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gpoEscolherPedido);
            this.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPedido";
            this.gpoEscolherPedido.ResumeLayout(false);
            this.gpoEscolherPedido.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudQuantidade)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudParcelas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gpoEscolherPedido;
        private System.Windows.Forms.NumericUpDown nudQuantidade;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown nudParcelas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvLista;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.ComboBox cboMercado;
        private System.Windows.Forms.ComboBox cboModoDePagamento;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtMarca;
        private System.Windows.Forms.ComboBox cboProduto;
        private System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtNomeRecebimento;
        private System.Windows.Forms.TextBox txtCep;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtNumeroCartao;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.Label lblTotalPorProduto;
        private System.Windows.Forms.Label label2;
    }
}