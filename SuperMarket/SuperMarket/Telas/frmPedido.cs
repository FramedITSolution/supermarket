﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperMarket.Telas
{
    public partial class frmPedido : Form
    {
        public frmPedido()
        {
            InitializeComponent();
            this.CarregarProdudos();
        }

        Model.ModelProduto pedido = new Model.ModelProduto();
        Business.BusinessPedido businessPedido = new Business.BusinessPedido();
        Model.ModelProduto modelProduto = new Model.ModelProduto();
        Business.BusinessProduto businessProduto = new Business.BusinessProduto();

        private void nudQuantidade_ValueChanged(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void CalcularProduto()
        {
            string produto = cboProduto.Text;
            string mercado = cboMercado.Text;

            Model.ModelProduto model = businessProduto.BuscaDosRegistros(produto, mercado);
            decimal preco = model.Preco;
            txtMarca.Text = model.Marca;
            txtPreco.Text = Convert.ToString(preco);

            int qtd = Convert.ToInt32(nudQuantidade.Value);
            decimal totalPorProduto = preco * qtd;
            lblTotalPorProduto.Text = Convert.ToString(totalPorProduto);

        }

        private void CarregarProdudos()
        {

            List<Model.ModelProduto> model = businessProduto.BuscarProduto();


            cboProduto.DisplayMember = nameof(Model.ModelProduto.NomeProduto);
            cboProduto.DataSource = model;

            cboMercado.DisplayMember = nameof(Model.ModelProduto.Mercado);
            cboMercado.DataSource = model;

            string produto = cboProduto.Text;
            string mercado = cboMercado.Text;

            Model.ModelProduto modelo = businessProduto.BuscaDosRegistros(produto, mercado);
            txtMarca.Text = modelo.Marca;
            txtPreco.Text = Convert.ToString(modelo.Preco);
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Close();
            frmMenu start = new frmMenu();
            start.Show();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtNumCasa.Text == "Número da casa")
                {
                    throw new ArgumentException("Informe o número da sua casa");
                }
                Model.ModelPedido pedido = new Model.ModelPedido();
                pedido.NomeParaReceber = txtNomeRecebimento.Text;
                pedido.Complemento = txtComplemento.Text;
                pedido.DataEntrega = dtpData.Value.Date;
                pedido.Horadopedido = DateTime.Now.TimeOfDay;
                pedido.ModoPagamento = cboModoDePagamento.Text;
                pedido.NomeParaReceber = txtNomeRecebimento.Text;
                pedido.PedidoCep = txtCep.Text;
                pedido.PedidoEndereço = txtEndereco.Text;
                pedido.PedidoNumeroCasa = Convert.ToInt32(txtNumCasa.Text);
                pedido.PedidoParcela = Convert.ToInt32(nudParcelas.Value);
                pedido.PedidoTotal = Convert.ToDecimal(lblTotal.Text);

                List<Model.ModelProdutoGridView> itens = dgvLista.DataSource as List<Model.ModelProdutoGridView> ?? new List<Model.ModelProdutoGridView>();
                businessPedido.SalvarPedido(pedido, itens);

                MessageBox.Show("Pedido salvo...");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro. Tente novamente mais tarde",
                    "Pedido", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }           
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                Model.ModelProduto p = cboProduto.SelectedItem as Model.ModelProduto;

                List<Model.ModelProdutoGridView> itens = dgvLista.DataSource as List<Model.ModelProdutoGridView> ?? new List<Model.ModelProdutoGridView>();
                itens.Add(new Model.ModelProdutoGridView
                {
                    Id = p.Id,
                    NomeProduto = p.NomeProduto,
                    Marca = p.Marca,
                    Mercado = p.Mercado,
                    Preco = p.Preco,
                    Qtd = Convert.ToInt32(nudQuantidade.Value)
                });

                dgvLista.DataSource = null;
                dgvLista.DataSource = itens;

                decimal totProduto = Convert.ToDecimal(lblTotalPorProduto.Text);
                decimal atual = Convert.ToDecimal(lblTotal.Text);
                decimal total = atual + totProduto;

                lblTotal.Text = Convert.ToString(total);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void txtNomeRecebimento_Enter(object sender, EventArgs e)
        {
            txtNomeRecebimento.BackColor = Color.FromArgb(224, 224, 224);
            txtNomeRecebimento.Text = string.Empty;
        }

        private void txtNomeRecebimento_Leave(object sender, EventArgs e)
        {
            txtNomeRecebimento.BackColor = Color.White;
            if(txtNomeRecebimento.Text == string.Empty)
            {
                txtNomeRecebimento.Text = "Nome para receber";
            }
        }

        private void txtEndereco_Enter(object sender, EventArgs e)
        {
            txtEndereco.BackColor = Color.FromArgb(224, 224, 224);
            txtEndereco.Text = string.Empty;
        }

        private void txtEndereco_Leave(object sender, EventArgs e)
        {
            txtEndereco.BackColor = Color.White;
            if (txtEndereco.Text == string.Empty)
            {
                txtEndereco.Text = "Endereço";
            }
        }

        private void txtCep_Enter(object sender, EventArgs e)
        {
            txtCep.BackColor = Color.FromArgb(224, 224, 224);
            txtCep.Text = string.Empty;
        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            txtCep.BackColor = Color.White;
            if (txtCep.Text == string.Empty)
            {
                txtCep.Text = "Cep";
            }
        }

        private void txtComplemento_Enter(object sender, EventArgs e)
        {
            txtComplemento.BackColor = Color.FromArgb(224, 224, 224);
            txtComplemento.Text = string.Empty;
        }

        private void txtComplemento_Leave(object sender, EventArgs e)
        {
            txtComplemento.BackColor = Color.White;
            if (txtComplemento.Text == string.Empty)
            {
                txtComplemento.Text = "Complemento";
            }
        }

        private void txtNumCasa_Enter(object sender, EventArgs e)
        {
            txtNumCasa.BackColor = Color.FromArgb(224, 224, 224);
            txtNumCasa.Text = string.Empty;
        }

        private void txtNumCasa_Leave(object sender, EventArgs e)
        {
            txtNumCasa.BackColor = Color.White;
            if (txtNumCasa.Text == string.Empty)
            {
                txtNumCasa.Text = "Número da casa";
            }
        }

        private void txtNumeroCartao_Enter(object sender, EventArgs e)
        {
            txtNumeroCartao.BackColor = Color.FromArgb(224, 224, 224);
            txtNumeroCartao.Text = string.Empty;
        }

        private void txtNumeroCartao_Leave(object sender, EventArgs e)
        {
            txtNumeroCartao.BackColor = Color.White;
            if (txtNumeroCartao.Text == string.Empty)
            {
                txtNumeroCartao.Text = "Card number";
            }
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.FromArgb(224, 224, 224);
            txtSenha.Text = string.Empty;
            txtSenha.UseSystemPasswordChar = true;
        }

        private void txtSenha_Leave(object sender, EventArgs e)
        {
            txtSenha.BackColor = Color.White;
            if (txtSenha.Text == string.Empty)
            {
                txtSenha.Text = "Password";
                txtSenha.UseSystemPasswordChar = false;
            }
        }

        private void lblFechar_Click_1(object sender, EventArgs e)
        {
            Close();
            frmMenu start = new frmMenu();
            start.Show();
        }

        private void cboProduto_MouseEnter(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void cboProduto_MouseLeave(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void cboMercado_MouseEnter(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void cboMercado_MouseLeave(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void gpoEscolherPedido_Move(object sender, EventArgs e)
        {
            this.CalcularProduto();
        }

        private void cboModoDePagamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboModoDePagamento.Text == "Cartão")
            {
                txtNumeroCartao.Enabled = true;
                txtSenha.Enabled = true;
            }
            else
            {
                txtNumeroCartao.Enabled = false;
                txtSenha.Enabled = false;
            }
        }

        private void cboMercado_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
